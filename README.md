# Generating docs from a bunch of markdown

## Install dependencies, set up mkdocs, and copy markdown files

```bash

poetry install # dependencies
poetry run mkdocs new <doc-name>
cp <existing-md-folder>/* <doc-name>/docs

```

## Configure mkdocs

In `<doc-folder>`, there is an `mkdocs.yml`. Add the following properties:

```yaml
repo_url: <path-to-remote-gitlab-project>
edit_uri: https://gitlab.com/-/ide/project/<gitlab-namespace>/edit/<default-branch>/-/<path-to-docs-folder>/docs
```

This will update the "edit on gitlab" button to link to the web IDE for the project

## Deploy to pages

Refer to [The gitlab CI file in this project](.gitlab-ci.yml) to see how to bulid the site and deploy to pages